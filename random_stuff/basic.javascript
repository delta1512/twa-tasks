var MY_GLOBAL_DICT = {
  'a' : 1,
  'b' : 2,
  'c' : 3
};

var MY_GLOBAL_LIST = [1, 2, 3];


function helloWorld(msg='Hello world') {
  console.log(msg);
}


function makeItBlue(obj) {
  obj.style.backgroundColor = '#0000FF';
  console.log('I made this blue: ');
  console.log(obj);
}


function addFormNums() {
  // what's wrong with this function?
  let n1 = Number(document.getElementById('num1').value);
  let n2 = Number(document.getElementById('num2').value);
  let val = n1 + n2;

  document.getElementById('form-msg').innerText = val;

  console.log(val);
}


function scopedExample() {
  let functionScope = 100;
  for (let forScope = 56; forScope < 59; forScope++) {
    console.log(forScope);
    console.log(functionScope);
    if (forScope == 58) {
      let ifScope = 666666;
      console.log("We entered the if");
      console.log(functionScope + ifScope + forScope);
    }
    //console.log(ifScope);
  }

  //console.log(ifScope);
  //console.log(forScope);
  console.log(functionScope);
}


function customFactorial(f, n=5) {
  let myFunction = f;
  console.log(myFunction(5));
}


function recursiveFactorial(n) {
  if (n == 1) {
    return 1;
  } else {
    return n*recursiveFactorial(n-1);
  }
}

function sequentialFactorial(n) {
  let sum = 1;
  while (n > 1) {
    sum *= n;
    n--;
  }
  return sum;
}
