<?php
  require_once("conn.php");
  require_once("validation.php");


  // Safely open a session
  // only starts the session if it is not started
  function sessionOpen() {
    if (isset($_SESSION)) {
      return;
    } else {
      session_start();
    }
  }


  function login($username, $password) {
    $conn = getDB();
    // Sanitise username before querying
    $username = $conn->escape_string(sanitise($username));
    if (!validate($username, "username")) {
      return false;
    }
    // Hash the password to check with the database
    $password = hash("sha256", $password);

    $sql = "SELECT category FROM membership ";
    $sql = $sql . "WHERE username='" . $username . "' AND ";
    $sql = $sql . "password='" . $password . "';";

    $result = $conn->query($sql) or SQLError($sql);
    $conn->close();

    $cat = $result->fetch_assoc();
    $cat = $cat["category"];
    return $cat;
  }


  function isLoggedIn() {
    sessionOpen();
    if (isset($_SESSION["username"]) && isset($_SESSION["password"])) {
      return login($_SESSION["username"], $_SESSION["password"]);
    }
    return false;
  }


  function getMID() {
    sessionOpen();
    if (isset($_SESSION["username"]) && isset($_SESSION["password"])) {
      $conn = getDB();
      // Sanitise username before querying
      $username = $conn->escape_string(sanitise($_SESSION["username"]));
      // Hash the password to check with the database
      $password = hash("sha256", $_SESSION["password"]);

      $sql = "SELECT member_id FROM membership ";
      $sql = $sql . "WHERE username='" . $username . "' AND ";
      $sql = $sql . "password='" . $password . "';";
      $result = $conn->query($sql) or SQLError($sql);
      $conn->close();
      if ($result->num_rows > 0) {
        $mid = $result->fetch_assoc();
        $mid = $mid["member_id"];
        return $mid;
      }
      return false;
    }
    return false;
  }


  function categoryToImg($cat) {
    $dir = "img/";
    switch($cat) {
      case "Free":
        return $dir . "free.svg";
      case "Premium":
        return $dir . "premium.svg";
      case "Family":
        return $dir . "family.svg";
      default:
        return $dir . "free.svg";
    }
  }
?>
