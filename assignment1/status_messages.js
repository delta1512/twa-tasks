/*
  status_messages.js
  By Marcus Belcastro (19185398)

  Allows for the clearing of user-feedback prompts for playlist.php
*/


function clearStatus(promptImg) {
  let section = promptImg.parentElement;
  // Clear the inner HTML to get rid of all children of the prompt
  // Solution source: https://stackoverflow.com/questions/3955229/remove-all-child-elements-of-a-dom-node-in-javascript
  section.innerHTML = '';
  // Hide the section of the prompt
  section.style.display = 'none';
}
