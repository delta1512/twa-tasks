<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Assignment 1 |
-->


<?php
  require_once("conn.php");
  require_once("loginOps.php");
  require_once("validation.php");

  // Variable tracks whether the call is POSTback
  $isPOSTback = isset($_POST["submit"]);
  // Holds the input in the search query box
  $searchInput = "";
  // How many results were found?
  $nResults = 0;

  if ($isPOSTback) {
    if (!empty($_POST["search-query"])) {
      $searchInput = $_POST["search-query"];
      // Sanitise the input
      $searchQuery = sanitise($searchInput);
      $conn = getDB();
      $searchQuery = $conn->escape_string($searchInput);
      // Prepare for insertion into the SQL statement
      $searchQuery = "'%" . $searchQuery . "%'";

      // Perform the queries
      $sql = "SELECT t.track_title, a.artist_name, al.album_name, ";
      $sql = $sql . "t.track_id, a.artist_id, al.album_id FROM track t ";
      $sql = $sql . "JOIN album al ON t.album_id=al.album_id ";
      $sql = $sql . "JOIN artist a ON al.artist_id=a.artist_id ";
      $sql = $sql . "WHERE t.track_title LIKE " . $searchQuery . ";";
      $resultsSongs = $conn->query($sql);

      $sql = "SELECT al.album_id, al.album_name, al.thumbnail, ";
      $sql = $sql . "a.artist_id, a.artist_name FROM album al ";
      $sql = $sql . "JOIN artist a ON al.artist_id=a.artist_id ";
      $sql = $sql . "WHERE al.album_name LIKE " . $searchQuery . ";";
      $resultsAlbums = $conn->query($sql);

      $sql = "SELECT a.artist_id, a.artist_name, a.thumbnail FROM artist a ";
      $sql = $sql . "WHERE a.artist_name LIKE " . $searchQuery . ";";
      $resultsArtists = $conn->query($sql);
      $conn->close();

      $nResults = $resultsSongs->num_rows + $resultsAlbums->num_rows + $resultsArtists->num_rows;
    }
  }
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Search - 24/7Music</title>
    <link rel="stylesheet" href="stylesheet.css">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png">
    <script src="validation.js"></script>
    <!-- Font sourced from fonts.google.com -->
    <link href="https://fonts.googleapis.com/css2?family=Cabin&display=swap" rel="stylesheet">
  </head>

  <body>
    <header>
      <nav>
        <a href="search.php" class="current-nav-page">Search</a>
        <a href="play.php">Play</a>
        <section class="login-logout-section">
          <?php
            if ($category = isLoggedIn()) {
              ?>
              <img src="<?php echo categoryToImg($category); ?>"
                      title="Membership class: <?php echo $category; ?>"
                      alt="membership class">
              <a href="login.php"><?php echo $_SESSION["username"]; ?></a>
              <span class="separator orange-text">|</span>
              <a href="logout.php">Logout</a>
              <?php
            } else {
              ?> <a href="login.php">Login</a> <?php
            }
          ?>
        </section>
        <a href="playlist.php">Playlist</a>
      </nav>

      <h1>Search</h1>
    </header>

    <article>
      <section class="form-section">
        <form action=<?php echo $_SERVER["PHP_SELF"]; ?> onsubmit="return submitValidate(this);" method="post">
          <input type="text" id="search-bar" name="search-query" placeholder="Search..."
                value="<?php echo $searchInput; ?>">
          </input>
          <input type="submit" id="search-button" name="submit" value="&#x1F50D"></input><br>
          <span class="submit-error"></span><br>
        </form>
      </section>

      <section class="search-results">
        <?php
        if ($nResults > 0) {
          // Showing the song group of results
          if ($resultsSongs->num_rows > 0) {
            ?>
            <h2>Songs</h2>
            <section class="result-section song-results">
            <?php
            while ($row = $resultsSongs->fetch_assoc()) {
            ?>
              <article>
                <img src="img/default_song.svg" alt="track image of <?php echo $row['track_title']; ?>">
                <p>
                  <a href="play.php?track=<?php echo $row['track_id']; ?>">
                    <?php echo $row['track_title']; ?>
                  </a>
                </p>
                <p>
                  <a href="play.php?album=<?php echo $row['album_id']; ?>">
                    <?php echo $row['album_name']; ?>
                  </a>
                </p>
                <p>
                  <a href="play.php?artist=<?php echo $row['artist_id']; ?>">
                    By <?php echo $row['artist_name']; ?>
                  </a>
                </p>
              </article>
            <?php
            } // end while
            ?> </section> <?php
          } // end if for song results
          // Showing the album group of results
          if ($resultsAlbums->num_rows > 0) {
            ?>
            <h2>Albums</h2>
            <section class="result-section album-results">
            <?php
            while ($row = $resultsAlbums->fetch_assoc()) {
            ?>
              <article>
                <img src="<?php echo getThumb($row["thumbnail"], "album"); ?>"
                  alt="album art of <?php echo $row['album_name'] ?>">
                <p>
                  <a href="play.php?album=<?php echo $row['album_id']; ?>">
                    <?php echo $row['album_name']; ?>
                  </a>
                </p>
                <p>
                  <a href="play.php?artist=<?php echo $row['artist_id']; ?>">
                    By <?php echo $row['artist_name']; ?>
                  </a>
                </p>
              </article>
            <?php
            } // end while
            ?> </section> <?php
          } // end if for album results
          // Showing the artist group of results
          if ($resultsArtists->num_rows > 0) {
            ?>
            <h2>Artists</h2>
            <section class="result-section artist-results">
            <?php
            while ($row = $resultsArtists->fetch_assoc()) {
            ?>
              <article>
                <img src="<?php echo getThumb($row["thumbnail"], "artist"); ?>"
                  alt="artist portrait of <?php echo $row['artist_name'] ?>">
                <p>
                  <a href="play.php?artist=<?php echo $row['artist_id']; ?>">
                    <?php echo $row['artist_name']; ?>
                  </a>
                </p>
              </article>
            <?php
            } // end while
            ?> </section> <?php
          } // end if for artist results
        } else if ($isPOSTback) { // else for if there are any results at all
          if (validate($searchInput, "search")) {
            ?>
            <p class="no-results-err-msg">No results were found.</p>
            <?php
          } else {
            ?>
            <p class="no-results-err-msg">Invalid search, try again.</p>
            <?php
          }
        } // end else $isPOSTback
        ?>
      </section>
    </article>

    <footer>
      <p class="orange-text">
        <a href="notes.html">
          24/7Music - Assignment 1 TWA - By Marcus Belcastro (19185398) - June 2020
        </a>
      </p>
    </footer>
  </body>
</html>
