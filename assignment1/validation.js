/*
  validation.js
  By Marcus Belcastro (19185398)

  Validates form inputs and prints error messages where necessary
*/


// All regex is original work
// All fields are required
let REGEX = {
  'playlist-name' : /^[0-9a-zA-Z@\._\- ]{1,30}$/,
  'search-bar' : /^[\w\W]{1,500}$/,
  'username' : /^[0-9a-zA-Z@\._\-]{1,100}$/,
  'password' : /^[\w\W]{1,100}$/,
  'id' : /^[0-9]{1,50}$/
};


// Messaging constants
let REQUIRED_MSG = 'This field is required';
let BAD_DATA = 'Invalid data entered';

let GENERAL_SUBMIT_ERROR = 'There are errors in the form, correct them and try again';


function isValid(obj) {
  let regex = REGEX[obj.id];
  if (regex) {
    return obj.value.match(regex);
  } else {
    // If a regex rule is not found, assume no validation necessary
    return true;
  }
}


// The function to place as `onblur` on text form inputs
function validateTextBox(textBox) {
  // If a textBox is disabled or softDisabled, it is assumed to be valid
  if (textBox.diabled) {
    clearErrors(textBox);
    return true;
  }

  // If the text box is a required field and there is nothing there, then warn the user
  if (!textBox.value) {
    handleErrors(textBox);
    return false;
  }

  if (!isValid(textBox)) {
    handleErrors(textBox, BAD_DATA);
    return false;
  }
  clearErrors(textBox);
  return true;
}


// The validations script for when the form is submitted
function submitValidate(form) {
  let allInputs = form.querySelectorAll('input');
  let allSelects = form.querySelectorAll('select');

  // Validate all text boxes
  for (let i = 0; i < allInputs.length; i++) {
    let obj = allInputs[i];
    if (obj.getAttribute('type') == 'text' && !obj.disabled) {
      if (!validateTextBox(obj)) {
        if (obj.id == "search-bar") {
          showSubmitError(form, "Enter a valid search term");
        } else {
          showSubmitError(form);
        }
        return false;
      }
    }
  }

  // Validate all combo boxes
  for (let i = 0; i < allSelects.length; i++) {
    let obj = allSelects[i];
    if (!obj.options[obj.selectedIndex].value) {
      showSubmitError(form, "Please select an item");
      return false;
    } else if (!obj.options[obj.selectedIndex].value.match(REGEX["id"])) {
      showSubmitError(form, "Please select a valid item");
      return false;
    }
  }

  return true;
}


// Sets a list of fields (or 1 field) to prompt a DOM error message
// To add a message to the erroneous fields, add an optional message argument
function handleErrors(elemList, message="") {
  if (!elemList.length) {
    elemList = [elemList];
  }
  for (let i = 0; i < elemList.length; i++) {
    if (message) {
      signalError(elemList[i], message);
    } else {
      signalError(elemList[i]);
    }
  }
}


function signalError(inputObj, message=REQUIRED_MSG) {
  // Set the label to be red if any
  if (inputObj.labels.length > 0) {
    inputObj.labels[0].style.color = 'red';
  }
  // Turn the border of the input box red
  inputObj.style.borderColor = 'red';
  // Present an error message to the text area next to each box
  inputObj.nextElementSibling.textContent = message;
}


function clearErrors(elemList) {
  if (!elemList.length) {
    elemList = [elemList];
  }
  for (let i = 0; i < elemList.length; i++) {
    clearError(elemList[i]);
  }
}


function clearError(inputObj) {
  if (inputObj.labels.length > 0) {
    inputObj.labels[0].style.color = 'black';
  }
  inputObj.style.borderColor = '';
  inputObj.nextElementSibling.textContent = '';
}


function showSubmitError(form, message=GENERAL_SUBMIT_ERROR) {
  let submitError = form.querySelectorAll(".submit-error");
  submitError[0].style.display = 'inline-block';
  submitError[0].innerHTML = message;
}


function clearSubmitError(form) {
  let submitError = form.querySelectorAll(".submit-error");
  submitError.style.display = "none";
}
