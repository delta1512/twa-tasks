<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Assignment 1 |


  The following php script generates the play.php page.

  It operates under a model where the content is created/fetched first and then
  inserted into the HTML document at specific places.
  The document consists of an informational area which presents the basic information
  of what was selected and then an additional content section below to display
  optional additional content such as songs or albums.
-->


<?php
  require_once("conn.php");
  require_once("loginOps.php");
  require_once("validation.php");

  // List of variables used for the output section
  // Input variables
  $trackInp = "";
  $albumInp = "";
  $artistInp = "";
  $playlistInp = "";
  // The source URL and alt attr. of the larger image to be displayed
  $imgSrc = "";
  $imgAlt = "";
  // The text content of the headings below the image
  // These are optional and will only display if not empty
  $hTop = "";
  $hMid = "";
  $hSubtitle = "";
  // The additional content placed below the main information of the result
  // This is HTML that is to be inserted into the correct position below
  $htmlContent = "";

  $conn = getDB();
  // IF REQUESTING TRACK
  if (!empty($_GET["track"])) {
    $trackInp = $conn->escape_string(sanitise($_GET["track"]));
    if (validate($trackInp, "track_id")) {
      $sql = "SELECT t.track_title, a.album_name, t.track_length, ";
      $sql = $sql . "t.spotify_track FROM track t ";
      $sql = $sql . "JOIN album a ON t.album_id=a.album_id ";
      $sql = $sql . "WHERE t.track_id=" . $trackInp . ";";
      $results = $conn->query($sql) or SQLError($sql);

      if ($results->num_rows > 0) {
        $row = $results->fetch_assoc();
        $hTop = $row["track_title"];
        $hMid = $row["album_name"];
        $imgSrc = "img/default_song.svg";
        $imgAlt = "song image of " . $row["track_title"];
        $hSubtitle = "Length: ";
        // Track length field can be null
        if (!empty($row["track_length"])) {
          $hSubtitle = $hSubtitle . $row["track_length"];
        } else {
          $hSubtitle = $hSubtitle . "N/A";
        }
        // Spotify track can be null
        if (!empty($row["spotify_track"])) {
          $htmlContent = '<iframe src="https://open.spotify.com/embed/track/'
            . $row["spotify_track"] . '" frameborder="0" allowtransparency="true"
            allow="encrypted-media"></iframe>';
        } else {
          $htmlContent = '<p class="no-results-err-msg">Cannot load music player.</p>';
        }
      } else {
        $htmlContent = '<p class="no-results-err-msg">Invalid ID provided.</p>';
      }
    } else {
      $trackInp = "";
    }

  // IF REQUESTING ALBUM
  } else if (!empty($_GET["album"])) {
    $albumInp = $conn->escape_string(sanitise($_GET["album"]));
    if (validate($albumInp, "album_id")) {
      $sql = "SELECT t.track_id, t.track_title, t.track_length, ";
      $sql = $sql . "a.artist_name, al.album_name, al.thumbnail FROM track t ";
      $sql = $sql . "JOIN album al ON t.album_id=al.album_id ";
      $sql = $sql . "JOIN artist a ON al.artist_id=a.artist_id ";
      $sql = $sql . "WHERE al.album_id=" . $albumInp . ";";
      $results = $conn->query($sql) or SQLError($sql);

      if ($results->num_rows > 0) {
        $row = $results->fetch_assoc();
        // The album name, artist name and thumbnail is loaded into the SQL join
        // so all rows will contain the same data
        $hTop = $row["album_name"];
        $hSubtitle = "By " . $row["artist_name"];
        $imgSrc = getThumb($row["thumbnail"], "album");
        $imgAlt = "album image of " . $row["album_name"];
        $htmlContent = '<h2>Songs</h2>';
        $htmlContent = $htmlContent . '<section class="result-section song-results">';
        while ($row) {
          // Track length field can be null
          if (!empty($row["track_length"])) {
            $len = $row["track_length"];
          } else {
            $len = "N/A";
          }
          $htmlContent = $htmlContent .
          '<article>
            <img src="img/default_song.svg" alt="track image of ' . $row["track_title"] . '">
            <p>
              <a href="play.php?track=' . $row["track_id"] . '">'
                . $row["track_title"] .
              '</a>
            </p>
            <p>Length: ' . $len . '</p>
          </article>';
          $row = $results->fetch_assoc();
        }
        $htmlContent = $htmlContent . "</section>";
      } else {
        $htmlContent = '<p class="no-results-err-msg">Invalid ID provided.</p>';
      }
    } else {
      $albumInp = "";
    }

  // IF REQUESTING ARTIST
  } else if (!empty($_GET["artist"])) {
    $artistInp = $conn->escape_string(sanitise($_GET["artist"]));

    if (validate($artistInp, "artist_id")) {
      $sql = "SELECT al.album_id, al.album_name, al.album_date, ";
      $sql = $sql . "al.thumbnail althumb, a.artist_name, a.thumbnail athumb FROM album al ";
      $sql = $sql . "JOIN artist a ON al.artist_id=a.artist_id ";
      $sql = $sql . "WHERE a.artist_id=" . $artistInp . ";";
      $results = $conn->query($sql) or SQLError($sql);

      if ($results->num_rows > 0) {
        $row = $results->fetch_assoc();
        // Artist details are loaded into the join statement
        $hTop = $row["artist_name"];
        $imgSrc = getThumb($row["athumb"], "artist");
        $imgAlt = "artist image of " . $row["artist_name"];
        $htmlContent = '<h2>Albums</h2>';
        $htmlContent = $htmlContent . '<section class="result-section album-results">';
        while ($row) {
          // Track length field can be null
          if (!empty($row["album_date"])) {
            $date = $row["album_date"];
          } else {
            $date = "N/A";
          }
          $htmlContent = $htmlContent .
          '<article>
            <img src="' . "/twa/thumbs/albums/" . $row["althumb"]
            . '" alt="album image of ' . $row["album_name"] . '">
            <p>
              <a href="play.php?album=' . $row["album_id"] . '">'
                 . $row["album_name"] .
              '</a>
            </p>
            <p>Date: ' . $date . '</p>
          </article>';
          $row = $results->fetch_assoc();
        }
        $htmlContent = $htmlContent . "</section>";
      } else {
        $htmlContent = '<p class="no-results-err-msg">This artist has made no songs.</p>';
      }
    } else {
      $htmlContent = '<p class="no-results-err-msg">Invalid artist provided.</p>';
    }

  // IF REQUESTING PLAYLIST
  } else if (!empty($_GET["playlist"])) {
    $playlistInp = $conn->escape_string(sanitise($_GET["playlist"]));

    if (validate($playlistInp, "playlist_id")) {
      $sql = "SELECT mp.playlist_name, t.track_id, t.track_title, ";
      $sql = $sql . "t.track_length FROM memberPlaylist mp ";
      $sql = $sql . "JOIN playlist p ON mp.playlist_id=p.playlist_id ";
      $sql = $sql . "JOIN track t ON p.track_id=t.track_id ";
      $sql = $sql . "WHERE mp.playlist_id=" . $playlistInp . ";";
      $results = $conn->query($sql) or SQLError($sql);

      if ($results->num_rows > 0) {
        $row = $results->fetch_assoc();

        $hTop = $row["playlist_name"];
        $imgSrc = "img/default_playlist.svg";
        $imgAlt = "playlist image of " . $row["playlist_name"];
        $htmlContent = '<h2>Songs</h2>';
        $htmlContent = $htmlContent . '<section class="result-section song-results">';
        while ($row) {
          // Track length field can be null
          if (!empty($row["track_length"])) {
            $len = $row["track_length"];
          } else {
            $len = "N/A";
          }
          $htmlContent = $htmlContent .
          '<article>
            <img src="img/default_song.svg" alt="track image of ' . $row["track_title"] . '">
            <p>
              <a href="play.php?track=' . $row["track_id"] . '">'
                . $row["track_title"] .
              '</a>
            </p>
            <p>Length: ' . $len . '</p>
          </article>';
          $row = $results->fetch_assoc();
        }
        $htmlContent = $htmlContent . "</section>";
      } else {
        $htmlContent = '<p class="no-results-err-msg">This playlist has no songs.</p>';
      }
    } else {
      $htmlContent = '<p class="no-results-err-msg">Invalid playlist provided.</p>';
    }
  }
  $conn->close();
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Play - 24/7Music</title>
    <link rel="stylesheet" href="stylesheet.css">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png">
    <!-- Font sourced from fonts.google.com -->
    <link href="https://fonts.googleapis.com/css2?family=Cabin&display=swap" rel="stylesheet">
  </head>

  <body>
    <header>
      <nav>
        <a href="search.php">Search</a>
        <a href="play.php" class="current-nav-page">Play</a>
        <section class="login-logout-section">
          <?php
            if ($category = isLoggedIn()) {
              ?>
              <img src="<?php echo categoryToImg($category); ?>"
                      title="Membership class: <?php echo $category; ?>"
                      alt="membership class">
              <a href="login.php"><?php echo $_SESSION["username"]; ?></a>
              <span class="separator orange-text">|</span>
              <a href="logout.php">Logout</a>
              <?php
            } else {
              ?> <a href="login.php">Login</a> <?php
            }
          ?>
        </section>
        <a href="playlist.php">Playlist</a>
      </nav>

      <?php
        if (!empty($trackInp)) {
          $type = "- Songs";
        } else if (!empty($albumInp)) {
          $type = "- Albums";
        } else if (!empty($artistInp)) {
          $type = "- Artists";
        } else {
          $type = "- Your Playlists";
        }
      ?>
      <h1>Play <?php echo $type; ?></h1>
    </header>

    <article>
      <section class="play-information">
        <?php
        // Only display the information if there is information to display
        // $imgSrc acts as a sentinel, it will always be set if a result was successful
        if (!empty($imgSrc)) {
          ?>
          <img src=<?php echo $imgSrc; ?> alt="<?php echo $imgAlt; ?>">
          <p class="heading-top"><?php echo $hTop; ?></p>
          <?php
          // Print the rest of the headings only if they exist
          if (!empty($hMid)) {
            ?> <p class="heading-mid"><?php echo $hMid; ?></p> <?php
          }
          if (!empty($hMid)) {
            ?> <p class="heading-sub"><?php echo $hSubtitle; ?></p> <?php
          }
        }
        ?>
      </section>
      <section class="search-results">
        <?php
        // If any one of the options is requested (GET args provided)
        if (!(empty($trackInp) and empty($albumInp) and empty($artistInp) and empty($playlistInp))) {
          echo $htmlContent;
        } else {
          ?>
          <p class="no-results-err-msg">Nothing was found.</p>
          <p class="no-results-err-msg">
            <a href="search.php">Search for an Artist, Song or Album</a>
          </p>
          <?php
        } // end else
        ?>
      </section>
      <?php
      // Set up the 'Add to playlist' button
      // Only add it if a song is selected and actually found
      if (!(empty($imgSrc) or empty($trackInp))) {
        // Only let it appear if a user is logged in
        // Otherwise we can't add the song to a playlist
        if (isLoggedIn()) {
          // First fetch all the possible playlists the user has
          $memberID = getMID();
          $sql = "SELECT playlist_id, playlist_name FROM memberPlaylist ";
          // Limit of 50 because of database size from other students
          $sql = $sql . "WHERE member_id=" . $memberID . " LIMIT 50;";
          $conn = getDB();
          $results = $conn->query($sql) or SQLError($sql);
          $conn->close();
          ?>
          <section class="form-section">
            <form action="playlist.php" method="post">
          <?php
          if ($results->num_rows > 0) {
            // The following select element lists all the playlists the user has
            // So that they can add a song to a playlist they own
            $buttVal = "Add to playlist";
            ?> <select id="playlist-dropdown" name="playlist"> <?php
            while ($row = $results->fetch_assoc()) {
              $pName = sanitise($row["playlist_name"]);
              if (empty($pName)) {
                $pName = "[No name]";
              }
              ?>
                <option value="<?php echo $row["playlist_id"]; ?>">
                  <?php echo $pName; ?>
                </option>
              <?php
            } // end while
            ?>
              </select>
              <!-- A hidden input tag to send the track_id to the playlist.php script -->
              <input type="hidden" name="track" value="<?php echo $trackInp; ?>"></input>
            <?php
          } else { // end check for rows > 0
            // If the user has no playlists, they must create one
            $buttVal = "Create a playlist";
            // Do not add any other elements so that the user can simply go to playlist.php
          } // end else check for rows > 0
          ?>
              <input type="submit" name="submit" value="<?php echo $buttVal; ?>"></input>
            </form>
          </section>
          <?php
        } // end isLoggedIn()
      } // end check for song
      ?>
    </article>

    <footer>
      <p class="orange-text">
        <a href="notes.html">
          24/7Music - Assignment 1 TWA - By Marcus Belcastro (19185398) - June 2020
        </a>
      </p>
    </footer>
  </body>
</html>
