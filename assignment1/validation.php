<?php
  // Renamed test_input() function sourced from w3Schools
  // Source: https://www.w3schools.com/php/php_form_validation.asp
  function sanitise($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

  // Gets the full thumbnail URL for a type of thumbnail if not null
  // else return the default image of the respective type.
  function getThumb($thumb, $type) {
    if ($type == "album") {
      if (!empty($thumb)) {
        return "/twa/thumbs/albums/" . $thumb;
      } else {
        return "img/default_album.svg";
      }
    } else if ($type == "artist") {
      if (!empty($thumb)) {
        return "/twa/thumbs/artists/" . $thumb;
      } else {
        return "img/default_artist.svg";
      }
    }
  }


  // Performs regex validation for a string of a particular type in the
  // $REGEX_FIELDS array
  function validate($str, $type) {
    // For some reason this only works when the $REGEX_FIELDS variable is in function scope
    // All regex is original work
    $REGEX_FIELDS = array(
      "playlist_name"=>"/^[0-9a-zA-Z@\._\- ]{1,30}$/",
      "username"=>"/^[0-9a-zA-Z@\._\-]{1,100}$/",
      "playlist_id"=>"/^[0-9]{1,50}$/",
      "member_id"=>"/^[0-9]{1,50}$/",
      "track_id"=>"/^[0-9]{1,50}$/",
      "artist_id"=>"/^[0-9]{1,50}$/",
      "album_id"=>"/^[0-9]{1,50}$/",
      "search"=>"/^[\w\W]{1,500}$/"
    );

    return preg_match($REGEX_FIELDS[$type], $str);
  }
?>
