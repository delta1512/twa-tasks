<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Assignment 1 |
-->

<?php
  // All that needs to happen is to destroy the session and redirect
  session_start();
  session_destroy();
  header("location: search.php");
?>

<!--
  Print the body of the document to display the background and prevent
  any non-user-friendly directives if something goes wrong.
-->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Logout - 24/7Music</title>
    <link rel="stylesheet" href="stylesheet.css">
  </head>

  <body>
    <p class"no-results-err-msg">
      Logging out... If you are not redirected in a few seconds, click
      <a href="search.php">this link</a> to go to the search page.
    </p>
  </body>
</html>
