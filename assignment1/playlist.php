<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Assignment 1 |
-->


<?php
  require_once("conn.php");
  require_once("loginOps.php");
  require_once("validation.php");

  // Display a message if set, it can be either success or fail
  // success will display green and fail will display red
  // Used for user feedback when submitting the forms on this page
  $prompt = "";
  $status = "success";
  // Whether the user is logged-in
  $login = isLoggedIn();
  if ($login) {
    $MID = getMID();
  } else {
    // if the user is not logged in and tries to access this page, redirect to login.php
    header("location: login.php");
  }

  $conn = getDB();

  if (isset($_POST["submit"]) and $login) {
    // If the form has a playlist ID and track ID, add the track to the playlist
    if (!(empty($_POST["playlist"]) or empty($_POST["track"]))) {
      // Sanitise the inputs
      $playlistID = $conn->escape_string(sanitise($_POST["playlist"]));
      $trackID = $conn->escape_string(sanitise($_POST["track"]));
      // Regex will validate the numeric properties automatically
      if (validate($playlistID, "playlist_id") and validate($trackID, "track_id")) {
        // Check if the playlist actually exists and if the person actually owns it
        $sql = "SELECT playlist_id FROM memberPlaylist ";
        $sql = $sql . "WHERE playlist_id=" . $playlistID;
        $sql = $sql . " AND member_id=" . $MID . ";";
        $results = $conn->query($sql) or SQLError($conn->error);
        if ($results->num_rows > 0) {
          $sql = "INSERT INTO playlist (playlist_id, track_id) VALUES (";
          $sql = $sql . $playlistID . ", " . $trackID . ");";
          if ($conn->query($sql) or SQLError($conn->error)) {
            $prompt = "Successfully added a song to the playlist";
            $status = "success";
          } else {
            $prompt = "Failed to add the song to the playlist";
            $status = "fail";
          }
        } else {
          $prompt = "The playlist specified does not exist";
          $status = "fail";
        }
      } else {
        $prompt = "Invalid playlist provided";
        $status = "fail";
      }

    // If the form has a name, create a new playlist and refresh the page
    } else if (!empty($_POST["playlist_name"])) {
      // Sanitise the inputs
      $playlistName = $conn->escape_string(sanitise($_POST["playlist_name"]));
      if (validate($playlistName, "playlist_name")) {
        $sql = "INSERT INTO memberPlaylist (member_id, playlist_name) VALUES (";
        $sql = $sql . $MID . ", '" . $playlistName . "');";
        if ($conn->query($sql) or SQLError($conn->error)) {
          $prompt = "Successfully created a new playlist";
          $status = "success";
        } else {
          $prompt = "Failed to create the new playlist";
          $status = "fail";
        }
      } else {
        $prompt = "Playlist name was invalid, try again";
        $status = "fail";
      }
    }
  }

  // fetch all the playlists for displaying
  // LIMIT is used to prevent too many playlists appearing from other students
  // This is at the end of the initial script in case an INSERT occurred
  if ($login) {
    $sql = "SELECT * FROM memberPlaylist WHERE member_id=" . $MID . " LIMIT 50;";
    $playlistQuery = $conn->query($sql) or SQLError($conn->error);
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Playlist - 24/7Music</title>
    <link rel="stylesheet" href="stylesheet.css">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png">
    <script src="status_messages.js"></script>
    <script src="validation.js"></script>
    <!-- Font sourced from fonts.google.com -->
    <link href="https://fonts.googleapis.com/css2?family=Cabin&display=swap" rel="stylesheet">
  </head>

  <body>
    <header>
      <nav>
        <a href="search.php">Search</a>
        <a href="play.php">Play</a>
        <section class="login-logout-section">
          <?php
            // The $login variable is set with the value necessary, prevent extra DB queries
            if ($category = $login) {
              ?>
              <img src="<?php echo categoryToImg($category); ?>"
                      title="Membership class: <?php echo $category; ?>"
                      alt="membership class">
              <a href="login.php"><?php echo $_SESSION["username"]; ?></a>
              <span class="separator orange-text">|</span>
              <a href="logout.php">Logout</a>
              <?php
            } else {
              ?> <a href="login.php">Login</a> <?php
            }
          ?>
        </section>
        <a href="playlist.php" class="current-nav-page">Playlist</a>
      </nav>

      <h1>Your Playlists</h1>
    </header>

    <article>
      <?php
        // Print the user feedback if it has been set
        if (!empty($prompt)) {
          ?>
          <section class="prompt <?php echo $status; ?>">
            <img src="img/cross.svg" alt="click to dismiss" onclick="clearStatus(this);">
            <p><?php echo $prompt; ?></p>
          </section>
          <?php
        }
      ?>
      <section class="search-results">
        <?php
          if ($login and $playlistQuery->num_rows > 0) {
            ?> <section class="result-section playlist-results"> <?php
            // If the user is logged in and there are playlists to list, list playlists
            while ($rowPlaylist = $playlistQuery->fetch_assoc()) {
              $pName = $conn->escape_string(sanitise($rowPlaylist['playlist_name']));
              if (empty($pName)) {
                $pName = "[This playlist has no name]";
              }
              ?>
                <article>
                  <img src="img/default_playlist.svg" alt="playlist image for <?php echo $rowPlaylist['playlist_name']; ?>">
                  <p>
                    <a href="play.php?playlist=<?php echo $rowPlaylist['playlist_id']; ?>">
                      <?php echo $pName; ?>
                    </a>
                  </p>
              <?php
              // Search for all songs related to the playlist name
              // This is for functionality part 5 of playlist.php where the user
              // can choose a song. It will appear as a list of recommended songs.
              // LIMIT is used to prevent spamming the user with too many recommendations
              $sql = "SELECT t.track_id, t.track_title FROM track t ";
              $sql = $sql . "JOIN album al ON t.album_id=al.album_id ";
              $sql = $sql . "JOIN artist a ON al.artist_id=a.artist_id ";
              $sql = $sql . "WHERE t.track_title LIKE '%" . $pName . "%' OR ";
              $sql = $sql . "al.album_name LIKE '%" . $pName . "%' OR ";
              $sql = $sql . "a.artist_name LIKE '%" . $pName . "%' LIMIT 25;";
              $results = $conn->query($sql) or SQLError($conn->error);
              // If no relevant songs were found, fetch some random ones
              // This can be improved with a popularity heuristic of some sort if available
              if ($results->num_rows < 1) {
                $sql = "SELECT track_id, track_title FROM track LIMIT 25;";
                $results = $conn->query($sql) or SQLError($conn->error);
              }
              // Build the select box for the add song form in each playlist article
              ?>
                  <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" onsubmit="return submitValidate(this);" method="post">
                    <select id="song-picker" name="track">
                      <option value="-1">Recommended songs...</option>
              <?php
              while ($rowTrack = $results->fetch_assoc()) {
                ?>
                      <option value="<?php echo $rowTrack["track_id"]; ?>">
                        <?php echo $rowTrack["track_title"]; ?>
                      </option>
                <?php
              } // end while $rowTrack
              ?>
                    </select>
                    <!-- Hidden input in order to include the playlist ID of the current playlist -->
                    <input type="hidden" name="playlist" value="<?php echo $rowPlaylist['playlist_id']; ?>"></input>
                    <input type="submit" name="submit" value="Add song"></input>
                    <span class="submit-error"></span>
                  </form>
                </article>
              <?php
            } // end while $rowPlaylist
            ?> </section> <?php
          } else { // else for $login and $playlistQuery
            ?> <p class="no-results-err-msg">You have no playlists. Create one using the form below.</p> <?php
          } // if for $login and $playlistQuery
        ?>
      </section>

      <section class="form-section">
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" onsubmit="return submitValidate(this);" method="post">
          <label for="playlist_name">Name of playlist</label>
          <input type="text" name="playlist_name" placeholder="My playlist" maxlength="30"
                  id="playlist-name" onblur="validateTextBox(this);"></input>
          <span class="err-msg"></span><br>
          <input type="submit" name="submit" value="Create playlist"></input><br>
          <span class="submit-error"></span>
        </form>
      </section>
    </article>

    <footer>
      <p class="orange-text">
        <a href="notes.html">
          24/7Music - Assignment 1 TWA - By Marcus Belcastro (19185398) - June 2020
        </a>
      </p>
    </footer>
  </body>
</html>

<?php
  // Close the connection to the database after everything is finished
  $conn->close();
?>
