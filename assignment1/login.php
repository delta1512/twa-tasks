<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Assignment 1 |
-->


<?php
require_once("conn.php");
require_once("loginOps.php");

$loginFail = "";

if (isset($_POST["submit"])) {
  // Attempt logging in the user
  // $category will signify as to whether the login was successful
  // $category is the category of the account (family, premium, free)
  if ($category = login($_POST["username"], $_POST["password"])) {
    session_start();
    $_SESSION["username"] = $_POST["username"];
    $_SESSION["password"] = $_POST["password"];
    // Redirect to search.php
    header("location: search.php");
  } else {
    $loginFail = "Incorrect username or password.";
  }
}
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Login - 24/7Music</title>
    <link rel="stylesheet" href="stylesheet.css">
    <link rel="shortcut icon" type="image/png" href="img/favicon.png">
    <script src="validation.js"></script>
    <!-- Font sourced from fonts.google.com -->
    <link href="https://fonts.googleapis.com/css2?family=Cabin&display=swap" rel="stylesheet">
  </head>

  <body>
    <header>
      <nav>
        <a href="search.php">Search</a>
        <a href="play.php">Play</a>
        <section class="login-logout-section">
          <?php
            if ($category = isLoggedIn()) {
              ?>
              <img src="<?php echo categoryToImg($category); ?>"
                      title="Membership class: <?php echo $category; ?>"
                      alt="membership class">
              <a href="login.php" class="current-nav-page"><?php echo $_SESSION["username"]; ?></a>
              <span class="separator orange-text">|</span>
              <a href="logout.php">Logout</a>
              <?php
            } else {
              ?> <a href="login.php" class="current-nav-page">Login</a> <?php
            }
          ?>
        </section>
        <a href="playlist.php">Playlist</a>
      </nav>

      <h1>Login</h1>
    </header>

    <article>
      <section class="form-section">
        <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" onsubmit="return submitValidate(this);" method="post">
          <label for="username">Username:</label>
          <input type="text" id="username" name="username" placeholder="Username"
                onblur="validateTextBox(this);"></input>
          <span class="err-msg"></span><br>
          <label for="password">Password:</label>
          <input type="password" id="password" name="password"
                onblur="validateTextBox(this);"></input>
          <span class="err-msg"></span><br>

          <input type="submit" name="submit" value="Login"></input><br>
          <span class="submit-error"></span>
        </form>
      </section>
      <?php
      if (!empty($loginFail)) {
        ?> <p class="no-results-err-msg"><?php echo $loginFail?></p> <?php
      }
      ?>
    </article>

    <footer>
      <p class="orange-text">
        <a href="notes.html">
          24/7Music - Assignment 1 TWA - By Marcus Belcastro (19185398) - June 2020
        </a>
      </p>
    </footer>
  </body>
</html>
