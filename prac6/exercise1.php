<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 7 Practical Exercises |
-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Week 7 Exercise 1</title>
  </head>
  <body>
    <?php
      //obtain the firstname input from the $_GET array
      $namestr = $_GET["firstname"];
      $email = $_GET["email"];
      $addr = $_GET["postaddr"];
      $sport = $_GET["favsport"];
      // Check if the maillist variable is set and store accordingly
      if (empty($_POST["emaillist"])) {
        $maillist = "No";
      } else {
        $maillist = "Yes";
      }
    ?>
    <p>The following information was received from the form:</p>
    <p><strong>name = </strong> <?php echo "$namestr"; ?></p>
    <p><strong>email = </strong> <?php echo "$email"; ?></p>
    <p><strong>address = </strong> <?php echo "$addr"; ?></p>
    <p><strong>fav sports = </strong> <?php echo "$sport"; ?></p>
    <p><strong>mail list consent = </strong> <?php echo "$maillist"; ?></p>
  </body>
</html>
