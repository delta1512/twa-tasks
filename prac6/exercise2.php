<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 7 Practical Exercises |
-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Week 7 Exercise 1</title>
  </head>
  <body>
    <?php
      // Fetch the variables
      $namestr = $_POST["firstname"];
      $email = $_POST["email"];
      $addr = $_POST["postaddr"];
      $sports = "";
      // Handle the set of favourite sports
      if (isset($_POST["favsport"])) {
        // Fix the off-by-one comma issue
        $sports = $_POST["favsport"][0];
        // Convert the array of fav sports to a comma-separated string
        for ($i = 1; $i < count($_POST["favsport"]); $i++) {
          $sports = $sports . "," . $_POST["favsport"][$i];
        }
      }

      // Check if the maillist variable is set and store accordingly
      if (empty($_POST["emaillist"])) {
        $maillist = "No";
      } else {
        $maillist = "Yes";
      }
    ?>
    <p>The following information was received from the form:</p>
    <p><strong>name = </strong> <?php echo "$namestr"; ?></p>
    <p><strong>email = </strong> <?php echo "$email"; ?></p>
    <p><strong>address = </strong> <?php echo "$addr"; ?></p>
    <p><strong>fav sports = </strong> <?php echo "$sports"; ?></p>
    <p><strong>mail list consent = </strong> <?php echo "$maillist"; ?></p>
  </body>
</html>
