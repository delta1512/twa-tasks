<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 7 Practical Exercises |
-->

<?php
  if (isset($_POST["submit"])) {
    // Fetch the variables
    $fname = $_POST["firstname"];
    $email = $_POST["email"];
    $addr = $_POST["postaddr"];
    $sports = "";
    // Handle the set of favourite sports
    if (isset($_POST["favsport"])) {
      // Fix the off-by-one comma issue
      $sports = $_POST["favsport"][0];
      // Convert the array of fav sports to a comma-separated string
      for ($i = 1; $i < count($_POST["favsport"]); $i++) {
        $sports = $sports . "," . $_POST["favsport"][$i];
      }
    }

    // Check if the maillist variable is set and store accordingly
    if (!isset($_POST["emaillist"])) {
      $maillist = "No";
    } else {
      $maillist = "Yes";
    }
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Week 7 Exercise 3 Form</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <h1>Week 7 Exercise 3 PHP form demo</h1>
    <form id="userinfo" action="exercise3.php" method="post">
      <p>Please fill in the following form. All fields are mandatory.</p>

      <p>
        <label for="fname">First Name:</label>
        <input type="text" id="fname" name="firstname">
      </p>

      <p>
        <label for="email">Email Address:</label>
        <input type="text" id="email" name="email">
      </p>

      <p>
        <label for="addr">Postal Address:</label>
        <textarea rows="5" cols="300" id="addr" name="postaddr"></textarea>
      </p>

      <p>
        <label for="favsport[]">Favourite sport: </label>
        <select id="sport" name="favsport[]" size="4" multiple>
            <option value="soccer">Soccer</option>
            <option value="cricket">Cricket</option>
            <option value="squash">Squash</option>
            <option value="golf">Golf</option>
            <option value="tennis">Tennis</option>
            <option value="basketball">Basketball</option>
            <option value="baseball">Baseball</option>
        </select>
      </p>

      <p>
        <label for="list">Add me to the mailing list</label>
        <input type="checkbox" id="list" name="emaillist" value="Yes">
      </p>

      <p><input type="submit" name="submit" value="submit"></p>
    </form>

    <section id="output">
    <?php
      if (isset($_POST["submit"])) {
        echo   '<h2>The following information was received from the form:</h2>';
        echo   '<p><strong>First Name: </strong>' . $fname . '</p>';
        echo   '<p><strong>Email: </strong>' . $email . '</p>';
        echo   '<p><strong>Address: </strong>' . $addr . '</p>';
        echo   '<p><strong>Favourite sports: </strong>' . $sports . '</p>';
        echo   '<p><strong>Email consent: </strong>' . $maillist . '</p>';
      }
    ?>
    </section>
  </body>
</html>
