<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 8 Practical Exercises |
-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Week 8 Exercise 4</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <?php
      if (isset($_POST["submit"])) {
        require_once("conn.php");

        $qty = $_POST["quantity"];

        // Handle if quantity specified is non-numeric
        if (!is_numeric($qty)) {
          die("<span class=\"errorMsg\">The value entered was not a number</span>");
        }

        $sql = "SELECT name, quantityInStock, price ";
        $sql = $sql . "FROM product ";
        $sql = $sql . "WHERE quantityInStock > " . $dbConn->escape_string($qty) . " ";
        $sql = $sql . "ORDER BY quantityInStock ASC;";

        $results = $dbConn->query($sql)
          or die ('Problem with query: ' . $dbConn->error);
        $dbConn->close();

        // Fetch the first result
        $row = $results->fetch_assoc();
        // If there are no results, print the appropriate message
        if (!$row) {
          die("<span class=\"errorMsg\">There are no products that have more than "
                . $qty . " in stock.</span>");
        }
    ?>
    <h1>Products with stock > <?php echo $qty?></h1>
    <table>
      <tr>
        <th>Name </th>
        <th>Quantity In Stock</th>
        <th>Price</th>
      </tr>
    <?php while ($row) { ?>
    <tr>
      <td><?php echo $row["name"]?></td>
      <td><?php echo $row["quantityInStock"]?></td>
      <td><?php echo "$" . strval($row["price"])?></td>
    </tr>
    <?php
          // First row is already removed before the loop so we need to do this at the end
          $row = $results->fetch_assoc();
        } // While loop

        // Only if the form was submitted correctly and there is a table, terminate the html table
        echo "</table>";
      } else { // If statement
        die("<span class=\"errorMsg\">No form data was submitted</span>");
      }
    ?>
  </body>
</html>
