<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 8 Practical Exercises |
-->

<?php
  $qty = "";
  $valError = false;
  if (isset($_POST["quantity"])) {
    $qty = $_POST["quantity"];
    // Check if the value is not a number or erroneous
    $valError = !is_numeric($_POST["quantity"]);
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <style>
      input[type="text"] {border: 1px solid black;}
    </style>
    <link rel="stylesheet" href="styles.css">
    <title>Week 8 Exercise 5 Form</title>
  </head>

  <body>
    <form id="exercise4Form" method="post" action="Exercise5.php">
      <h1>Quantity in Stock</h1>
      <p>Please enter the quantity to check against stock levels</p>
      <p>
        <label for="quantity">Quantity: </label>
        <input type="text" name="quantity" size="10" id="quantity"
          maxlength="6" value="<?php echo $qty ?>">
        <?php
          // If the quantity is invalid, print error message
          if ($valError) {
            echo "<span class=\"errorMsg\">The value entered was not a number</span>";
          }
        ?>
      </p>
      <p><input type="submit" name="submit"></p>
    </form>
    <?php
      // POSTback, check if submission occurred
      if (isset($_POST["submit"])) {
        require_once("conn.php");

        if ($valError) {
          // Do nothing, the error message will already display above
          die();
        }

        $sql = "SELECT name, quantityInStock, price ";
        $sql = $sql . "FROM product ";
        $sql = $sql . "WHERE quantityInStock > " . $dbConn->escape_string($qty) . " ";
        $sql = $sql . "ORDER BY quantityInStock ASC;";

        $results = $dbConn->query($sql)
          or die ('Problem with query: ' . $dbConn->error);
        $dbConn->close();
        $row = $results->fetch_assoc();

        if (!$row) {
          die("<span class=\"errorMsg\">There are no products that have more than "
                . $qty . " in stock.</span>");
        }
        echo "<h1>Products with stock > " . $qty . "</h1>";
    ?>
    <table>
      <tr>
        <th>Name </th>
        <th>Quantity In Stock</th>
        <th>Price</th>
      </tr>
      <?php while ($row) { ?>
      <tr>
        <td><?php echo $row["name"]?></td>
        <td><?php echo $row["quantityInStock"]?></td>
        <td><?php echo "$" . strval($row["price"])?></td>
      </tr>
      <?php
          $row = $results->fetch_assoc();
        } // While loop
      // If POSTback, then a table is printed and requires termination
      echo "</table>";
    } // If statement
    ?>
  </body>
</html>
