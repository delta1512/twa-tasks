<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 8 Practical Exercises |
-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Week 8 Exercise 3</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <?php
      require_once("conn.php");

      $sql = "SELECT name, quantityInStock, price ";
      $sql = $sql . "FROM product ";
      $sql = $sql . "WHERE quantityInStock > 10 ";
      $sql = $sql . "ORDER BY quantityInStock ASC;";

      $results = $dbConn->query($sql)
        or die ('Problem with query: ' . $dbConn->error);
      $dbConn->close();
    ?>
    <h1>Products with stock > 10</h1>
    <table>
      <tr>
        <th>Name</th>
        <th>Quantity In Stock</th>
        <th>Price</th>
      </tr>
      <?php
        while ($row = $results->fetch_assoc()) { ?>
        <tr>
          <td><?php echo $row["name"]?></td>
          <td><?php echo $row["quantityInStock"]?></td>
          <td><?php echo "$" . strval($row["price"])?></td>
        </tr>
      <?php } // While loop ?>
    </table>
  </body>
</html>
