/*
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 5 Practical Exercises |
*/

var REGEX = {
  'surname' : /^[a-zA-Z -]{1,50}$/, // Original work
  'firstname' : /^[a-zA-Z -]{1,50}$/, // Original work
  'dob' : /^[0-9]{2,2}\/[0-9]{2,2}\/[0-9]{4,4}$/, // Original work
  'addrln1' : /^$|^[a-zA-Z0-9 -\/]{1,100}$/, // Original work
  'addrln2' : /^$|^[a-zA-Z0-9 -\/]{1,100}$/, // Original work
  'suburb' : /^$|^[a-zA-Z -]{1,50}$/, // Original work
  'postcode' : /^$|^[0-9]{4,4}$/, // Original work
  'dayphone' : /^$|^[0-9]{8,8}$|^[0-9]{10,10}$/, // Original work
  'mobile' : /^$|^04[0-9]{8,8}$/, // Original work
  'workphone' : /^$|^[0-9]{8,8}$|^[0-9]{10,10}$/, // Original work
  'email' : /^$|^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/   // From https://www.regular-expressions.info/email.html
};

var REQUIRED = [
  'title', 'surname', 'firstname', 'dob'
];


// Messaging constants
var REQUIRED_MSG = 'This field is required';
var AT_LEAST_1_REQUIRED = "At least one of these fields is required"
var BAD_DATA = 'Invalid data entered';

var GENERAL_SUBMIT_ERROR = 'There are errors in the form, please correct the highlighted fields.'


function isValid(obj) {
  // Separate the prefixes from any ids
  // This allows for emergency and next of kin details to be validated using
  // existing rules
  let objId = normaliseStr(obj.id);
  let regex = REGEX[objId];
  if (regex) {
    return obj.value.match(regex);
  } else {
    // If a regex rule is not found, assume no validation necessary
    return true;
  }
}


function isRequired(obj) {
  return REQUIRED.includes(normaliseStr(obj.id));
}


// The function to place as `onblur` on text form inputs
function validateTextBox(textBox) {
  // If a textBox is disabled or softDisabled, it is assumed to be valid
  if (textBox.diabled) {
    clearErrors(textBox);
    return true;
  }

  // If the text box is a required field and there is nothing there, then warn the user
  if (isRequired(textBox) && !textBox.value) {
    handleErrors(textBox);
    return false;
  }

  if (!isValid(textBox)) {
    handleErrors(textBox, BAD_DATA);
    return false;
  }
  clearErrors(textBox);
  return true;
}


function onChangeTitle(titleSelectionBox) {
  let otherBox = document.getElementById('othertitle');
  clearErrors(otherBox);

  console.log(titleSelectionBox.value);

  if (titleSelectionBox.value == 'Other') {
    otherBox.style.display = 'inline';
    otherBox.previousElementSibling.style.display = 'inline-block';
  } else {
    otherBox.style.display = 'none';
    otherBox.previousElementSibling.style.display = 'none';
  }
}


function validateOtherTitle(otherBox) {
  if (document.getElementById('title').value == 'Other' && !otherBox.value) {
    handleErrors(otherBox);
    return false;
  }
  clearErrors(otherBox);
  return true;
}


// The onchange function for the 'as-above' checkbox
function onCheckAsAbove(checkbox) {
  let nextKinInputs = checkbox.parentElement.querySelectorAll('input');
  // If box is checked,
  // Disable next of kin elements and copy values over
  if (checkbox.checked) {
    clearErrors(nextKinInputs);
    for (let i = 1; i < nextKinInputs.length; i++) {
      nextKinInputs[i].value = document.getElementById('em-' + normaliseStr(nextKinInputs[i].id)).value;
    }
  // Else remove the values
  } else {
    for (let i = 0; i < nextKinInputs.length; i++) {
      nextKinInputs[i].value = '';
    }
  }
}


// For the 3 phone number inputs onblur
function validate3Numbers(phoneField) {
  let inputs = phoneField.parentElement.querySelectorAll('input');
  let atLeast1 = false;
  for (let i = 0; i < inputs.length; i++) {
    if (!validateTextBox(inputs[i])) {
      return false;
    }
    atLeast1 = atLeast1 || inputs[i].value;
  }

  if (!atLeast1) {
    handleErrors(inputs, AT_LEAST_1_REQUIRED);
    return false;
  }

  return true;
}


// The validations script for when the form is submitted
function submitValidate(form) {
  let allInputs = form.querySelectorAll('input');

  for (let i = 0; i < allInputs.length; i++) {
    let obj = allInputs[i];
    if (obj.getAttribute('type') == 'text' && !obj.disabled) {
      if (!validateTextBox(obj)) {
        showSubmitError();
        return false;
      }
    }
  }

  // mobile field is selected here because we need to only specify 1 of the fields
  if (!validate3Numbers(document.getElementById('mobile'))) {
    showSubmitError();
    return false;
  }

  if (!validateOtherTitle(document.getElementById('othertitle'))) {
    showSubmitError();
    return false;
  }

  return true;
}


// Sets a list of fields (or 1 field) to prompt a DOM error message
// To add a message to the erroneous fields, add an optional message argument
function handleErrors(elemList, message="") {
  if (!elemList.length) {
    elemList = [elemList];
  }
  for (let i = 0; i < elemList.length; i++) {
    if (message) {
      signalError(elemList[i], message);
    } else {
      signalError(elemList[i]);
    }
  }
}


function signalError(inputObj, message=REQUIRED_MSG) {
  inputObj.labels[0].style.color = 'red';
  inputObj.style.borderColor = 'red';
  inputObj.nextElementSibling.textContent = message;
}


function clearErrors(elemList) {
  if (!elemList.length) {
    elemList = [elemList];
  }
  for (let i = 0; i < elemList.length; i++) {
    clearError(elemList[i]);
  }
}


function clearError(inputObj) {
  inputObj.labels[0].style.color = 'black';
  inputObj.style.borderColor = '';
  inputObj.nextElementSibling.textContent = '';
}


function showSubmitError(message=GENERAL_SUBMIT_ERROR) {
  let submitError = document.getElementById('submit-error');
  submitError.style.display = 'inline-block';
}


function clearSubmitError() {
  document.getElementById('submit-error').style.display = "none";
}


function normaliseStr(str) {
  str = str.split('-');
  return str[str.length-1];
}
