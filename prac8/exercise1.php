<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 10 Practical Exercises |
-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Week 10 Exercise 1</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <?php
      if (!empty($_GET["staffID"])) {
        $sid = $_GET["staffID"];

        $dbConn = new mysqli("localhost", "TWA_student", "TWA_2020_Autumn", "electrical");
        if($dbConn->connect_error) {
          die("Failed to connect to database " . $dbConn->connect_error);
        }

        // Note: usure if activity brief required purchase.id or purchase.orderID
        $sql = "SELECT p.id, p.orderDate, p.shippingDate, s.staffName ";
        $sql = $sql . "FROM purchase p ";
        $sql = $sql . "JOIN staff s ON p.staffID=s.staffID ";
        $sql = $sql . "WHERE p.staffID='";
        $sql = $sql . $dbConn->escape_string($sid) . "' ";
        $sql = $sql . "ORDER BY p.orderDate ASC;";

        $results = $dbConn->query($sql) or die('Problem with query: ' . $dbConn->error);
        $dbConn->close();
    ?>
    <h1>Purchases</h1>
    <?php
      if ($results->num_rows > 0) {
    ?>
    <table>
      <tr>
        <th>Order ID</th>
        <th>Order date</th>
        <th>Shipping date</th>
        <th>Staff name</th>
      </tr>
      <?php while ($row = $results->fetch_assoc()) { ?>
      <tr>
        <td><?php echo $row["id"]?></td>
        <td><?php echo $row["orderDate"]?></td>
        <td><?php echo $row["shippingDate"]?></td>
        <td><?php echo $row["staffName"]?></td>
      </tr>
      <?php } // while loop ?>
    </table>
    <?php
      } else { // if-statement for count()
        echo "<p>There are no results to show</p>";
      }
    } else { // if-statement for empty()
      echo "<p>No staffID provided</p>";
    }
    ?>
  </body>
</html>
