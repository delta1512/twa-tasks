<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 10 Practical Exercises |
-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Week 10 Exercise 2</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <p>Click a link to find orders placed by the staff member:</p>
    <?php
      $dbConn = new mysqli("localhost", "TWA_student", "TWA_2020_Autumn", "electrical");
      if($dbConn->connect_error) {
        die("Failed to connect to database " . $dbConn->connect_error);
      }

      $sql = "SELECT staffID, staffName FROM staff;";

      $results = $dbConn->query($sql) or die ('Problem with query: ' . $dbConn->error);
      $dbConn->close();
      $endpoint = "exercise1.php";

      while ($row = $results->fetch_assoc()) {
        echo "<p><a href=" . $endpoint . "?staffID=" . $row["staffID"]
              . ">" . $row["staffName"] . "</a></p>";
      }
    ?>
  </body>
</html>
