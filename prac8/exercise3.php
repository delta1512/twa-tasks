<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 10 Practical Exercises |
-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Week 10 Exercise 3</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <a href="exercise3a.php">Let's visit the second page</a>
    <?php
      if (isset($_POST["personName"]) && isset($_POST["hobby"])) {
        session_start();
        $_SESSION["name"] = $_POST["personName"];
        $_SESSION["hobby"] = $_POST["hobby"];
      }
    ?>
  </body>
</html>
