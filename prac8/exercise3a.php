<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 10 Practical Exercises |
-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Week 10 Exercise 3a</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <a href="exercise3a.php">Let's visit the second page</a>
    <?php
      session_start();

      if (isset($_SESSION["name"]) and isset($_SESSION["hobby"])) {
        echo "<p>Hello " . htmlspecialchars($_SESSION["name"]) . ", your hobby is "
              . htmlspecialchars($_SESSION["hobby"]) . "</p>";
        echo "<a href='exercise3.html'>Go to the starting form</a>";
      } else {
        // Redirect to the initial page if the session doesn't exist
        header("location: exercise3.html");
      }

      session_destroy();
    ?>
  </body>
</html>
