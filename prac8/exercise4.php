<!--
  Document by Marcus Belcastro (19185398) |

  TWA class: KW @ Tue 4pm with Kieran Luken |

  TWA Week 10 Practical Exercises |
-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Week 10 Exercise 4</title>
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <?php
      if (!empty($_POST["customerID"])) {

        $custId = $_POST["customerID"];

        $dbConn = new mysqli("localhost", "TWA_student", "TWA_2020_Autumn", "electrical");
        if($dbConn->connect_error) {
          die("Failed to connect to database " . $dbConn->connect_error);
        }

        $sql = "SELECT firstName, lastName, suburb FROM customer ";
        $sql = $sql . "WHERE customerID='";
        $sql = $sql . $dbConn->escape_string($custId) . "';";

        $results = $dbConn->query($sql) or die ('Problem with query: ' . $dbConn->error);
        $dbConn->close();

        if ($results->num_rows < 1) {
          echo "<p>No customer with ID: $custId found</p>";
        } else {
          echo "<p>Customer $custId details</p>";
          $row = $results->fetch_assoc();
    ?>
    <form>
        <label for="fname">Firstname:</label>
        <input type="text" value="<?php echo $row["firstName"]?>" name="fname"></input>
        <br>

        <label for="lastname">Surname:</label>
        <input type="text" value="<?php echo $row["lastName"]?>" name="lastname"></input>
        <br>

        <label for="suburb">Suburb:</label>
        <input type="text" value="<?php echo $row["suburb"]?>" name="suburb"></input>
        <br>
    </form>
    <?php
        } // Else from displaying the form
      } else { // if-statement with empty()
        echo "<p>No customer ID provided</p>";
      }
    ?>
  </body>
</html>
